class SayController < ApplicationController
  def hello
    @time = Time.now
  end

  def goodbay
    @time = 1.hour.from_now
    @files = Dir.glob('*')
  end
end
