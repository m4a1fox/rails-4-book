# NOTES FROM THE BOOK
---------------------

## Chapter 1 notes:
-------------------

Get all rails version on the computer
    
    $ gem list --local rails
    
Read file in command line

    $ tail -f [file path]

Default `Rails` database is `SQLite3`

Rails work with such database: `SQLite3`, `DB2`, `MySQL`, `Oracle`, `Postgres`, `Firebird`, `SQL Server`

Create Rails own documentation

    $ rails new dummy_app
    $ cd dummy_app
    $ rake doc:rails